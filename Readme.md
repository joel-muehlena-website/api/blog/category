# Category Service

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=joel-muehlena-website_category&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=joel-muehlena-website_category)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=joel-muehlena-website_category&metric=coverage)](https://sonarcloud.io/summary/new_code?id=joel-muehlena-website_category)
[![made-with-Go](https://img.shields.io/badge/Made%20with-Go-1f425f.svg)](https://go.dev/)

This is a service for the joel.muehlena.de website. More precisely for the blog. This service is to manage the categories for blog entries and allows to perform CRUD operations for them. The categories are stored in a PostgreSQL database.

[My Website](https://joel.muehlena.de)

[My Blog](https://blog.joel.muehlena.de)

## Test

I use `richgo` for testing because it looks a lot nicer 😁. But feel free to use `go test` instead. For running the test locally run `richgo test ./... -v -coverprofile=cover.out -covermode=atomic`. In the gitlab ci the test are run the first. The tests create a coverprofile which can be converted to e.g. html with `go tool cover -html=cover.out -o coverage.html`.

The ci pipeline also includes a SonarQube job. If you want to use it please look in the sonarqube documentation how to set it up.

## Running

I normally run all services in a Kubernetes Cluster. But it will work the same with docker or docker-compose.
