DROP TABLE IF EXISTS category;

CREATE TABLE category (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  createdat timestamp,
  updatedat timestamp DEFAULT now(),
  UNIQUE(name)
);

CREATE OR REPLACE FUNCTION preventUpdateCreatedAtForCategory() RETURNS trigger AS $PREVENTUPDATECREATEDATFORCATEGORY$
BEGIN
  if(NEW.createdat != OLD.createdat) {
    NEW.createdat = OLD.createdat;
  }

  return NEW;
END;
$PREVENTUPDATECREATEDATFORCATEGORY$ LANGUAGE plpgsql;

CREATE TRIGGER preventUpdateCreatedAtForCategoryTrigger BEFORE UPDATE ON category
  FOR EACH ROW EXECUTE PROCEDURE preventUpdateCreatedAtForCategory();