package main

import (
	//configServer "gitlab.com/joel-muehlena-website/utility/jm-config-server-addon-go"
	service "gitlab.com/joelMuehlena/go-ms-toolkit"
)

func main() {
	/*configServerUrl := "http://localhost:8081/config"
	filepath := "blog/category.dev.yaml"

	if os.Getenv("JM_SVC_ENV") == "production" {
		configServerUrl = "http://config-server.default.svc.cluster.local:8081/config"
		filepath = "blog/category.prod.yaml"
	}

	resultChannel := make(chan configServer.Result, 1)
	configServer.FetchConfig(configServerUrl, resultChannel, configServer.WithFilePath(filepath), configServer.WithLogging(configServer.ERROR))

	result := <-resultChannel

	if result.Error != nil {
		println(result.Error.Error())
		os.Exit(1)
	}*/

	svc := service.New(3000)

	svc.Name = "categroy-svc"

	getCategoryEndpoint := service.Endpoint[getCategoryRequest, getCategoryResonse]{
		Path:    "/category",
		Method:  service.GET,
		Handler: getCategory,
	}

	svc.RegisterEndpoint(&getCategoryEndpoint)

	svc.Run()
}
