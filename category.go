package main

import "time"

type Category struct {
	Id        int       `db:"id" json:"id"`
	Name      string    `db:"name" json:"name" validate:"required"`
	CreatedAt time.Time `db:"createdat" json:"createdAt" validate:"omitempty"`
	UpdatedAt time.Time `db:"updatedat" json:"updatedAt" validate:"omitempty"`
}
