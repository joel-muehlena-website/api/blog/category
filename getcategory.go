package main

import "time"

type getCategoryRequest struct{}

type getCategoryResonse struct {
	Category
}

func getCategory(gcr *getCategoryRequest) (*getCategoryResonse, int, error) {
	//return nil, 404, &HTTPError{Code: 404, Message: "category not found", Errors: []string{}}
	return &getCategoryResonse{Category{Id: 23, Name: "test", CreatedAt: time.Now(), UpdatedAt: time.Now()}}, 200, nil
}
